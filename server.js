const express = require("express");
const bodyParser = require("body-parser");
const diff = require('pdf-diff')
const chrome = require('selenium-webdriver/chrome');
const {Builder, By, Key, until} = require('selenium-webdriver');
const fs = require('fs');
const fsPromises = fs.promises;
const https = require('https');
const Promise = require("promise");
const get = require("async-get-file");
const path = require('path');
var cron = require('node-cron');

require('dotenv').config();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));


//Node.js server listener
app.listen(5000, () => {
    console.log("App listening on port 5000!")

    cron.schedule('1-59 * * * *', () => {
        selenium();
    });

});


app.get( "/index", (req, res) => {
    res.json({ result: 'success', message: 'The node.js application is running.' });
});


app.get( "/selenium", async(req, res) => {

    await selenium();

    res.json({result: 'success', message: 'Finish selenium'});

});


async function selenium(){

    /***********
     * Workflow

     1. Get lastRevision date value
     2. Compare lastRevision value with the value from "lastRevision.json"
     3. Pseudo logic

     RUN CRON JOB WITHIN A PERIOD {
             if( lastRevision value != "lastRevision.json" value ) {
                A. Crawl all pdfs from webpage, store the pdf file to "new_pdf" folder
                B. Compare with the pdfs from the "old_pdf" folder that have the same file name
                C. Send email if there is a difference between the pdfs
                D. Replace all old pdfs in "old_pdf" folder by the pdfs from new_pdf in "new_pdf" folder
                E. Set "lastRevision.json" value = current lastRevision value
             } else {
                //Assume PDFs no update in the page, do nothing.
             }
         }

     ***********/

    let oldRevision = await fsPromises.readFile('lastRevision.json', 'binary');
    oldRevision = JSON.parse(oldRevision);
    oldRevision = oldRevision.lastRevision;


    /********* CRON JOB LOGIC *********/
    let driver;
    const screen = {width: 640, height: 480};

    //Headless browser mode
    driver = await new Builder().forBrowser('chrome').setChromeOptions(new chrome.Options().headless().windowSize(screen)).build();

    let pdfs = [];

    try {

        await driver.get('file:///C:/Users/Ken%20Ip/Desktop/test-selenium/dummy.html');
        var lastRevision = await driver.wait(until.elementLocated(By.id('div_revisiondate')), 10000).getText();

        if(lastRevision !== oldRevision) {

            console.log("New version found. Running crawl process....");

            //A. Crawl all pdfs from webpage, store the pdf file to "new_pdf" folder

            /***** Website version *****/
            //let lists = await driver.findElement(By.xpath('//*[@id="sizecontrol"]/table/tbody/tr[3]/td/table/tbody/tr/td[2]/table[4]/tbody/tr/td[2]/div/ul/li[2]/ul'));
            /***** Text only version *****/
            let lists = await driver.findElement(By.xpath('/html/body/div[2]/ul/li[2]/ul'));
            let items = await lists.findElements(By.tagName('li'));

            let count = 0;
            for (item of items) {
                let pdf = await item.findElement(By.tagName('a')).getAttribute("href");
                let name = await item.getText();
                name = name.replace("[PDF]", "").trim();

                pdfs.push(name);
                console.log(name);
                await main(pdf,name);
                console.log("======= Downloaded =======");

                count++;
            }

            console.log("----END----");


            //B. Compare with the pdfs from the "old_pdf" folder that have the same file name

            //console.log(pdfs);

            console.log("---- PDF Compare Start ----");
            let diff_pdf = [];
            for (const ele of pdfs) {
                console.log("---- Comparing : "+ele);
                let result = await diff(`./old_pdf/${ele}.pdf`, `./new_pdf/${ele}.pdf`)

                if (typeof (result) !== 'undefined') {
                    //console.log('diffs:', diffs)
                    console.log('This file is not same:' + ele);
                    diff_pdf.push(ele);
                }

                console.log("------------------");
            }

            console.log(diff_pdf);
            console.log("----END----");



            //C. Send email if there is a difference between the pdfs

            if( diff_pdf.length > 0) {
                let email_list = await fsPromises.readFile('email.json', 'binary');
                email_list = JSON.parse(email_list);

                let transporter = EmailTransporter();

                let emailSubject = "PDF Differences Report"

                let emailText = "These pdf had been updated: \n" + diff_pdf.toString() + "\n\n";
                emailText += "Please check the updated version pdf here : ";
                emailText += "https://www.devb.gov.hk/en/construction_sector_matters/tender_notices/forecast_of_consultancies_and_tenders/tenders/2021_quarter_2_to_2022_quarter_1/index.html";
                emailText += "\n\n Best regards, \n IT support";

                for (const emailAddress of email_list.email) {
                    await sendEmail(transporter, emailAddress, emailSubject, emailText);
                }
            }

            //D. Replace all old pdfs in "old_pdf" folder by the pdfs from new_pdf in "new_pdf" folder
            for (const ele of pdfs) {
                fs.copyFile(`./new_pdf/${ele}.pdf`, `./old_pdf/${ele}.pdf`, (err) => {
                    if (err) throw err;
                    console.log('new pdf file was copied to old_pdf folder');
                });
            }

            //E. Set "lastRevision.json" value = current lastRevision value
            let updateRevision = {
                lastRevision: lastRevision
            };

            let data = JSON.stringify(updateRevision);
            fs.writeFileSync('lastRevision.json', data);



        }

    } catch(e) {
        console.log("!!!!!!!!!!SOMETHING ERROR!!!!!!!!!!");
        console.log(e);
    } finally {
        await driver.quit();
    }

}



async function main(link, name){
    var url = link;
    var options = {
        directory: "./new_pdf/",
        filename: `${name}.pdf`
    }
    await get(url,options);
}



function EmailTransporter(){
    var nodemailer = require('nodemailer');
    var transporter = nodemailer.createTransport({
        //service: 'gmail',
        port: process.env.SMTP_PORT,
        host: process.env.SMTP_HOST,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PWD
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    return transporter;
}


async function sendEmail(transporter, email, subject, text){
    var mailOptions = {
        from: process.env.SMTP_SENDER,
        to: email,
        subject: subject,
        text: text
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            res.json({response_code: 401, result: "ERROR", error: error});
        } else {
            console.log('Email sent: ' + info.response);
            res.json({response_code: 200, result: "SUCCESS", SENT: info.response});
        }
    });
}





app.get( "/diff", (req, res) => {
    diff('./a.pdf', './b.pdf')
    .then(diffs => {

        if (typeof (diffs) !== 'undefined') {
            //console.log('diffs:', diffs)

            console.log("======================");
            console.log( ((diffs[0][1].Texts))  );
            console.log("-----------------------");
            //console.log( ((diffs[0][1].Texts)[1][1].R)[0]  );
            console.log("======================");

            res.json({result: 'success', message: 'PDF differences.'});
        } else {
            res.json({result: 'success', message: 'NO Differences.'});
        }

    })
    .catch(err => {
        console.error('err:', err)
    })
});


